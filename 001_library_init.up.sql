BEGIN;


CREATE TABLE IF NOT EXISTS books
(
    id          UUID       NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
    title       TEXT       NOT NULL,
    author      TEXT       NOT NULL,
    genre       TEXT       NOT NULL,
    price       float      NOT NULL,
    description TEXT       NOT NULL,
    image       TEXT       NOT NULL,
    created_at  TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE USER_ROLE AS ENUM ('consumer', 'administrator');

CREATE TABLE IF NOT EXISTS users
(
    id          UUID        NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
    login       TEXT        NOT NULL,
    password    TEXT        NOT NULL,
    role        USER_ROLE   NOT NULL,
    token       TEXT        NULL,
    created_at  TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

COMMIT;
